
from src.tables import Tables as T

# class of names for query parameters that are not table fields
class var():
    distance = "distance"
    val_minutes = "val_minutes"
    id_routes = "var_id_routes"
    id_operators = "var_id_operators"
    id_prizes = "var_id_prizes"
    id_objs = "var_id_objs"
    id_users = "var_id_users"
    id_tags = "var_id_tags"

class Query():
    """Class used to perform queries on the database using the execute method
    that accept as argument the query code and the variable parts of the query.
    The codes, enumerated by the code class, are keys in the _code_to_query
    dict and the related value is the query to  be executed, and also keys in
    the _code_to_attribute dict whose values are tuples with the columns to be
    returned in case of select queries, or the columns to be inserted in case
    of insert queries.
    """
    class code():
        GET_TAGS, GET_OBJ, GET_OBJS, GET_ROUTE, \
        GET_ROUTES, GET_ROUTES_IN_CITY, \
        GET_ROUTES_IN_RADIUS, GET_ROUTES_FROM_OBJ, \
        GET_ROUTES_OF_OPERATOR, GET_ROUTE_OPERATOR, \
        GET_PRIZE_OPERATOR, \
        GET_OBJS_FROM_ROUTES, \
        GET_OPERATORS_INFO, \
        GET_ROUTES_FROM_OBJS_OF_USER, GET_OBJ_FROM_VERIFICATION, \
        GET_ROUTE_PRIZES, \
        GET_PRIZES_FROM_ROUTES, GET_NOT_WON_PRIZES_FROM_ROUTES, \
        GET_USER_BY_LOGIN, GET_USER_BY_FB, \
        GET_WON_PRIZE_OF_USER, \
        GET_COMPL_PRIZE_BY_CODE, GET_MANDATORY_OBJS_OF_PRIZE, \
        GET_INFO_FROM_ROUTE_PRIZE_OF_OPERATOR, GET_OBJS_TAGS, GET_ROUTES_TAGS,\
        GET_USERS_TAGS, \
        GET_WON_PRIZE_DATE_BY_VAL_CODE, \
        ADD_COMPLETE_OBJ, ADD_COMPLETE_PRIZE, ADD_OBJ, \
        ADD_ROUTE, ADD_PRIZE, ADD_COMPL_INFO, ADD_ROUTE_PRIZE_INFO, \
        ADD_MANDATORY_OBJS_TO_INFO, ADD_OBJ_TO_ROUTE, ADD_USER, \
        ADD_ARCHIVE_PRIZE, ADD_TAGS_TO_ROUTE, ADD_TAGS_TO_USER, \
        UPD_OPERATOR, \
        UPD_FB_USER, \
        UPD_PRIZE_VALIDATION, UPD_ROUTE, \
        UPD_PRIZE, UPD_OBJ, UPD_INFO_PROBABILITY, DEL_COMPL_PRIZE, \
        DEL_OBJ_FROM_ROUTE, DEL_PRIZE_FROM_ROUTE, DEL_MAND_OBJ_FROM_INFO, \
        DEL_MAND_OBJS_FROM_ROUTE, \
        DEL_TAGS_FROM_ROUTE, \
        DEL_TAGS_FROM_USER, \
        DEL_ROUTE, DEL_PRIZE = range(57)

    var = var

    # class of placeholder names for variable query parts
    class _var():
        id_obj = '%({0})s'.format(T.F.id_obj)
        id_route = '%({0})s'.format(T.F.id_route)
        id_prize = '%({0})s'.format(T.F.id_prize)
        id_operator = '%({0})s'.format(T.F.id_operator)
        id_info = '%({0})s'.format(T.F.id_info)
        id_user = '%({0})s'.format(T.F.id_user)
        fb_id = '%({0})s'.format(T.F.fb_id)
        city = '%({0})s'.format(T.F.city)
        pos_lat = '%({0})s'.format(T.F.position_latitude)
        pos_lng = '%({0})s'.format(T.F.position_longitude)
        distance = '%({0})s'.format(var.distance)
        val_code = '%({0})s'.format(T.F.validation_code)
        val_minutes = '%({0})s'.format(var.val_minutes)
        name_operator = '%({0})s'.format(T.F.name_operator)
        val_exp = '%({0})s'.format(T.F.validation_expiry_time)
        name = '%({0})s'.format(T.F.name)
        desc = '%({0})s'.format(T.F.description)
        val_days = '%({0})s'.format(T.F.validity_days)
        visible = '%({0})s'.format(T.F.visible)
        rep_days = '%({0})s'.format(T.F.repeat_days)
        prob = '%({0})s'.format(T.F.probability)
        login = '%({0})s'.format(T.F.login)

    # format dict used to inject table and field names in queries
    _fdict = {'User': T.User, 'Obj': T.Objective, 'Route': T.Route, 'R_a': T.Route_available, 'obj_route': T.obj_route,
              'Completion_obj': T.Completion_obj, 'route_prize_info': T.route_prize_info,
              'Completion_prize': T.Completion_prize, 'Prize': T.Prize, 'Archive': T.Archive, 'Tag': T.Tag,
              'tag_route': T.tag_route, 'tag_user': T.tag_user, 'Completion_info': T.Completion_info,
              'Mandatory_obj': T.Mandatory_obj, 'P_i': T.Prize_info, 'P_a': T.Prize_available,
              'id': T.F.id, 'id_route': T.F.id_route, 'id_obj': T.F.id_obj, 'id_prize': T.F.id_prize,
              'id_tag': T.F.id_tag,
              'id_operator' : T.F.id_operator, 'id_user': T.F.id_user, 'id_info': T.F.id_info, 'fb_id': T.F.fb_id,
              'city': T.F.city, 'login': T.F.login, 'psw': T.F.psw, 'salt': T.F.salt, 'is_operator': T.F.is_operator,
              'name_operator': T.F.name_operator, 'name': T.F.name, 'description': T.F.description,
              'position_latitude': T.F.position_latitude, 'position_longitude': T.F.position_longitude,
              'date': T.F.date, 'validity_days': T.F.validity_days, 'validation_code': T.F.validation_code,
              'visible': T.F.visible, 'validation_expiry_time': T.F.validation_expiry_time,
              'repeat_days': T.F.repeat_days, 'completion_date': T.F.completion_date, 'probability': T.F.probability,
              'deleted': T.F.deleted, 'validation_method': T.F.validation_method,
              'var_id_obj': _var.id_obj, 'var_id_route' : _var.id_route, 'var_city': _var.city,
              'var_pos_lat' : _var.pos_lat, 'var_pos_lng': _var.pos_lng, 'var_distance' : _var.distance,
              'var_id_operator' : _var.id_operator, 'var_id_user': _var.id_user, 'var_val_code': _var.val_code,
              'var_fb_id': _var.fb_id, 'var_id_prize': _var.id_prize, 'var_val_minutes': _var.val_minutes,
              'var_name_operator': _var.name_operator, 'var_val_exp': _var.val_exp, 'var_name': _var.name,
              'var_desc': _var.desc, 'var_val_days': _var.val_days, 'var_visible': _var.visible,
              'var_rep_days': _var.rep_days, 'var_prob': _var.prob, 'var_id_info': _var.id_info,
              'var_login': _var.login}

    # in select queries, {x} are used as placeholders for multiple parameters,
    # for example in queries like "... where attr in ({0}) ..."
    # they are used instead of %s because the latter causes an error when it is
    # substituted with a string of comma separated values using the
    # cursor.execute function.
    # as for insert queries, they are built using _code_to_attributes when
    # the execute method is called
    _code_to_query = {
        code.GET_TAGS : "select %(id)s as tag from %(Tag)s" % _fdict,
        code.GET_OBJ : "select * from %(Obj)s where %(id)s = %(var_id_obj)s" % _fdict,
        code.GET_OBJS : "select * from %(Obj)s" % _fdict,
        code.GET_ROUTE : "select * from %(R_a)s where %(id)s = %(var_id_route)s" % _fdict,
        code.GET_ROUTES : "select * from %(R_a)s" % _fdict,
        code.GET_ROUTES_IN_CITY : """
            select * from %(R_a)s
            where %(id)s IN
            (
                select %(id_route)s
                from %(obj_route)s JOIN %(Obj)s ON %(id_obj)s = %(id)s
                where %(city)s = %(var_city)s
            )""" % _fdict,
        code.GET_ROUTES_IN_RADIUS : """
            select * from %(R_a)s
            where %(id)s IN
            (
                select %(id_route)s
                from %(obj_route)s JOIN %(Obj)s ON %(id_obj)s = %(id)s
                where distance(%(var_pos_lat)s, %(var_pos_lng)s, position_latitude, position_longitude)
                        < %(var_distance)s
            )""" % _fdict,
        code.GET_ROUTES_FROM_OBJ : """
            select * from %(R_a)s
            where %(id)s IN
            (
                select %(id_route)s from %(obj_route)s
                where %(id_obj)s = %(var_id_obj)s
            )""" % _fdict,
        code.GET_ROUTES_OF_OPERATOR : """
            select * from %(R_a)s
            where %(id_operator)s = %(var_id_operator)s
            """ % _fdict,
        code.GET_ROUTE_OPERATOR : """
            select %(id_operator)s from %(Route)s
            where %(id)s = %(var_id_route)s
            """ % _fdict,
        code.GET_PRIZE_OPERATOR : """
            select %(id_operator)s from %(Prize)s
            where %(id)s = %(var_id_prize)s
            """ % _fdict,
        code.GET_OBJS_FROM_ROUTES : """
            select %(id_route)s, %(id_obj)s, %(name)s, %(description)s, %(city)s, %(position_latitude)s,
                %(position_longitude)s, %(validation_method)s
            from %(obj_route)s JOIN %(Obj)s ON %(id)s = %(id_obj)s
            where %(id_route)s in ({var_id_routes})
            """ % _fdict,
        code.GET_OPERATORS_INFO : """
            select %(login)s, %(name_operator)s, %(position_latitude)s,
                %(position_longitude)s
            from %(User)s
            where %(login)s IN ({var_id_operators})
            """ % _fdict,
        code.GET_ROUTES_FROM_OBJS_OF_USER : """
            select distinct %(id_route)s, %(id_obj)s
            from %(Completion_obj)s NATURAL JOIN %(obj_route)s
                JOIN %(R_a)s ON %(id_route)s = %(id)s
            where %(id_user)s = %(var_id_user)s AND now() <= ADDDATE(%(date)s, %(validity_days)s)
            """ % _fdict,
        code.GET_OBJ_FROM_VERIFICATION : """
            select %(id)s from %(Obj)s
            where %(validation_code)s = %(var_val_code)s
            """ % _fdict,
        code.GET_ROUTE_PRIZES: """
            select %(id_prize)s, %(id_info)s
            from %(route_prize_info)s
            where %(id_route)s = %(var_id_route)s
            """ % _fdict,
        code.GET_PRIZES_FROM_ROUTES : """
            select * from %(P_i)s
            where %(id_route)s IN ({var_id_routes})
            """ % _fdict,
        code.GET_NOT_WON_PRIZES_FROM_ROUTES : """
            select * from %(P_i)s
            where %(id_route)s IN ({var_id_routes}) AND
                %(id_prize)s NOT IN
                    (select %(id_prize)s from %(Completion_prize)s
                     where %(id_user)s = %(var_id_user)s
                    )
                AND %(id_prize)s NOT IN
                    (select %(id_prize)s
                     from %(Archive)s JOIN %(P_a)s P ON %(id_prize)s = P.%(id)s
                     where now() < ADDDATE(%(completion_date)s, %(repeat_days)s) AND %(id_user)s = %(var_id_user)s
                    )
            """ % _fdict,
        code.GET_USER_BY_LOGIN : """
            select %(login)s, %(psw)s, %(salt)s, %(is_operator)s, %(name_operator)s,
                %(position_latitude)s, %(position_longitude)s
            from %(User)s where %(login)s = %(var_login)s
            """ % _fdict,
        code.GET_USER_BY_FB : """
            select %(login)s, %(psw)s, %(salt)s, %(is_operator)s, %(name_operator)s,
                %(position_latitude)s, %(position_longitude)s
            from %(User)s where %(fb_id)s = %(var_fb_id)s
            """ % _fdict,
        code.GET_WON_PRIZE_OF_USER : """
            select %(id_prize)s from %(Completion_prize)s
            where %(id_prize)s = %(var_id_prize)s AND %(id_user)s = %(var_id_user)s
            """ % _fdict,
        code.GET_COMPL_PRIZE_BY_CODE : """
            select %(id_prize)s, %(id_user)s, %(id_operator)s, %(name)s, %(description)s, %(visible)s, %(repeat_days)s
            from %(Completion_prize)s JOIN %(Prize)s ON %(id_prize)s = %(id)s
            where %(validation_code)s = %(var_val_code)s AND
                now() <= ADDTIME(%(validation_expiry_time)s, '0:%(var_val_minutes)s:0')
            """ % _fdict,
        code.GET_MANDATORY_OBJS_OF_PRIZE : """
            select %(id_prize)s, %(id_obj)s
            from %(Mandatory_obj)s NATURAL JOIN %(route_prize_info)s
            where %(id_prize)s IN ({var_id_prizes})
            """ % _fdict,
        code.GET_INFO_FROM_ROUTE_PRIZE_OF_OPERATOR : """
            select %(id_info)s
            from %(route_prize_info)s JOIN %(Prize)s ON %(id_prize)s = %(id)s
            where %(id_route)s = %(var_id_route)s and %(id_prize)s = %(var_id_prize)s
                and %(id_operator)s = %(var_id_operator)s
            """ % _fdict,
        code.GET_OBJS_TAGS : """
            select %(id_obj)s AS %(id)s, %(id_tag)s
            from %(tag_obj)s
            where %(id_obj)s IN ({var_id_objs})
            """,
        code.GET_ROUTES_TAGS : """
            select %(id_route)s AS %(id)s, %(id_tag)s
            from %(tag_route)s
            where %(id_route)s IN ({var_id_routes})
            """ % _fdict,
        code.GET_USERS_TAGS : """
            select %(id_user)s AS %(id)s, %(id_tag)s
            from %(tag_user)s
            where %(id_user)s IN ({var_id_users})
            """ % _fdict,
        code.GET_WON_PRIZE_DATE_BY_VAL_CODE : """
            select %(completion_date)s
            from %(Archive)s
            where %(validation_code)s = %(var_val_code)s
            order by %(completion_date)s desc
            limit 1
            """ % _fdict,
        code.ADD_COMPLETE_OBJ: "insert into %(Completion_obj)s" % _fdict,
        code.ADD_COMPLETE_PRIZE: "insert into %(Completion_prize)s" % _fdict,
        code.ADD_OBJ : "insert into %(Obj)s" % _fdict,
        code.ADD_ROUTE : "insert into %(Route)s" % _fdict,
        code.ADD_PRIZE : "insert into %(Prize)s" % _fdict,
        code.ADD_COMPL_INFO : "insert into %(Completion_info)s" % _fdict,
        code.ADD_ROUTE_PRIZE_INFO : "insert into %(route_prize_info)s" % _fdict,
        code.ADD_MANDATORY_OBJS_TO_INFO : "insert into %(Mandatory_obj)s" % _fdict,
        code.ADD_OBJ_TO_ROUTE : "insert into %(obj_route)s" % _fdict,
        code.ADD_USER : "insert into %(User)s" % _fdict,
        code.ADD_ARCHIVE_PRIZE : "insert into %(Archive)s" % _fdict,
        code.ADD_TAGS_TO_ROUTE : "insert into %(tag_route)s" % _fdict,
        code.ADD_TAGS_TO_USER : "insert into %(tag_user)s" % _fdict,
        code.UPD_OPERATOR: """
            update %(User)s
            set %(name_operator)s = %(var_name_operator)s, %(position_latitude)s = %(var_pos_lat)s,
                %(position_longitude)s = %(var_pos_lng)s
            where %(login)s = %(var_id_operator)s
            """ % _fdict,
        code.UPD_FB_USER : """
            update %(User)s
            set %(fb_id)s = %(var_fb_id)s
            where %(login)s = %(var_id_user)s
            """ % _fdict,
        code.UPD_PRIZE_VALIDATION : """
            update %(Completion_prize)s
            set %(validation_expiry_time)s = %(var_val_exp)s, %(validation_code)s = %(var_val_code)s
            where %(id_prize)s = %(var_id_prize)s AND %(id_user)s = %(var_id_user)s
            """ % _fdict,
        code.UPD_ROUTE : """
            update %(Route)s
            set %(name)s = %(var_name)s, %(description)s = %(var_desc)s, %(validity_days)s = %(var_val_days)s
            where %(id)s = %(var_id_route)s
            """ % _fdict,
        code.UPD_PRIZE : """
            update %(Prize)s
            set %(name)s = %(var_name)s, %(description)s = %(var_desc)s, %(visible)s = %(var_visible)s,
                %(repeat_days)s = %(var_rep_days)s
            where %(id)s = %(var_id_prize)s AND %(id_operator)s = %(var_id_operator)s
            """ % _fdict,
        code.UPD_OBJ : """
            update %(Obj)s
            set %(name)s = %(var_name)s, %(description)s = %(var_desc)s, %(city)s = %(var_city)s,
                %(position_latitude)s = %(var_pos_lat)s, %(position_longitude)s = %(var_pos_lng)s
            where %(id)s = %(var_id_obj)s
            """ % _fdict,
        code.UPD_INFO_PROBABILITY : """
            update %(Completion_info)s
            set %(probability)s = %(var_prob)s
            where %(id)s = %(var_id_info)s
            """ % _fdict,
        code.DEL_COMPL_PRIZE : """
            delete from %(Completion_prize)s
            where %(id_prize)s = %(var_id_prize)s and %(id_user)s = %(var_id_user)s
            """ % _fdict,
        code.DEL_OBJ_FROM_ROUTE : """
            delete from %(obj_route)s
            where %(id_obj)s IN ({var_id_objs}) AND %(id_route)s = %(var_id_route)s
            """ % _fdict,
        code.DEL_PRIZE_FROM_ROUTE : """
            delete from %(route_prize_info)s
            where %(id_prize)s = %(var_id_prize)s AND %(id_route)s = %(var_id_route)s AND
                %(id_prize)s IN
                (
                    select %(id)s from %(Prize)s
                    where %(id_operator)s = %(var_id_operator)s
                )
            """ % _fdict,
        code.DEL_MAND_OBJ_FROM_INFO : """
            delete from %(Mandatory_obj)s
            where %(id_obj)s IN ({var_id_objs}) AND %(id_info)s = %(var_id_info)s
            """ % _fdict,
        code.DEL_MAND_OBJS_FROM_ROUTE : """
            delete from %(Mandatory_obj)s
            where %(id_info)s IN
                (
                    select %(id_info)s from %(route_prize_info)s
                    where %(id_route)s = %(var_id_route)s
                )
            """ % _fdict,
        code.DEL_TAGS_FROM_ROUTE : """
            delete from %(tag_route)s
            where %(id_route)s = %(var_id_route)s and %(id_tag)s IN ({var_id_tags})
            """ % _fdict,
        code.DEL_TAGS_FROM_USER : """
            delete from %(tag_user)s
            where %(id_user)s = %(var_id_user)s and %(id_tag)s IN ({var_id_tags})
            """ % _fdict,
        code.DEL_ROUTE : """
            update %(Route)s
            set %(deleted)s = 1
            where %(id)s in ({var_id_routes}) and %(id_operator)s = "%(var_id_operator)s"
            """ % _fdict,
        code.DEL_PRIZE : """
            update %(Prize)s
            set %(deleted)s = 1
            where %(id)s in ({var_id_prizes}) and %(id_operator)s = "%(var_id_operator)s"
            """ % _fdict
    }

    # This dictionary holds the query code as key and a tuple of the column
    # names for the query as value. If the value is not a tuple, then it equals
    # to a query code, meaning that the two queries have the same columns. If
    # so, the attributes are fetched when _get_query_and_attributes method is
    # called by execute.
    _code_to_attributes = {
        code.GET_TAGS : (T.F.id_tag,),
        code.GET_OBJ : (T.F.id, T.F.name, T.F.description, T.F.city,
                        T.F.position_latitude, T.F.position_longitude, T.F.validation_method),
        code.GET_OBJS : code.GET_OBJ,
        code.GET_ROUTE : (T.F.id, T.F.name),
        code.GET_ROUTES :
            (T.F.id_route, T.F.id_operator, T.F.name, T.F.description, T.F.validity_days),
        code.GET_ROUTES_IN_CITY : code.GET_ROUTES,
        code.GET_ROUTES_IN_RADIUS : code.GET_ROUTES,
        code.GET_ROUTES_FROM_OBJ : code.GET_ROUTES,
        code.GET_ROUTES_OF_OPERATOR : code.GET_ROUTES,
        code.GET_OPERATORS_INFO : (T.F.login, T.F.name_operator,
            T.F.position_latitude, T.F.position_longitude),
        code.GET_ROUTE_OPERATOR : (T.F.id_operator,),
        code.GET_PRIZE_OPERATOR : code.GET_ROUTE_OPERATOR,
        code.GET_OBJS_FROM_ROUTES : (T.F.id_route, T.F.id_obj, T.F.name, T.F.description, T.F.city,
                                     T.F.position_latitude, T.F.position_longitude, T.F.validation_method),
        code.GET_OBJ_FROM_VERIFICATION : (T.F.id,),
        code.GET_ROUTES_FROM_OBJS_OF_USER : (T.F.id_route, T.F.id_obj),
        code.GET_ROUTE_PRIZES : (T.F.id_prize, T.F.id_info),
        code.GET_PRIZES_FROM_ROUTES :
            (T.F.id_route, T.F.id_prize, T.F.name, T.F.description, T.F.visible,
             T.F.repeat_days, T.F.validation_method, T.F.id_operator, T.F.name_operator,
             T.F.position_latitude, T.F.position_longitude, T.F.id_info, T.F.probability,
             T.F.id_obj),
        code.GET_NOT_WON_PRIZES_FROM_ROUTES :
            code.GET_PRIZES_FROM_ROUTES,
        code.GET_USER_BY_LOGIN :
            (T.F.login, T.F.psw, T.F.salt, T.F.is_operator, T.F.name_operator,
             T.F.position_latitude, T.F.position_longitude),
        code.GET_USER_BY_FB : code.GET_USER_BY_LOGIN,
        code.GET_WON_PRIZE_OF_USER : (T.F.id_prize,),
        code.GET_MANDATORY_OBJS_OF_PRIZE : (T.F.id_prize, T.F.id_obj),
        code.ADD_COMPLETE_OBJ: (T.F.id_obj, T.F.id_user, T.F.date),
        code.ADD_COMPLETE_PRIZE: (T.F.id_prize, T.F.id_user, T.F.expiry_date),
        code.GET_COMPL_PRIZE_BY_CODE :
            (T.F.id_prize, T.F.id_user, T.F.id_operator, T.F.name, T.F.description,
             T.F.visible, T.F.repeat_days),
        code.GET_INFO_FROM_ROUTE_PRIZE_OF_OPERATOR : (T.F.id_info,),
        code.GET_OBJS_TAGS : (T.F.id, T.F.id_tag),
        code.GET_ROUTES_TAGS : code.GET_OBJS_TAGS,
        code.GET_USERS_TAGS : code.GET_OBJS_TAGS,
        code.GET_WON_PRIZE_DATE_BY_VAL_CODE : (T.F.completion_date,),
        code.ADD_OBJ : (T.F.name, T.F.description, T.F.city, T.F.position_latitude,
                        T.F.position_longitude, T.F.validation_method,
                        T.F.validation_code),
        code.ADD_ROUTE : (T.F.name, T.F.description, T.F.id_operator, T.F.validity_days),
        code.ADD_PRIZE : (T.F.id_operator, T.F.name, T.F.description, T.F.visible,
                          T.F.repeat_days, T.F.validation_method),
        code.ADD_COMPL_INFO : tuple(),
        code.ADD_ROUTE_PRIZE_INFO : (T.F.id_route, T.F.id_prize, T.F.id_info),
        code.ADD_MANDATORY_OBJS_TO_INFO : (T.F.id_info, T.F.id_obj),
        code.ADD_OBJ_TO_ROUTE : code.GET_ROUTES_FROM_OBJS_OF_USER,
        code.ADD_USER : code.GET_USER_BY_LOGIN,
        code.ADD_ARCHIVE_PRIZE : (T.F.id_prize, T.F.id_user, T.F.completion_date,
                T.F.validation_code),
        code.ADD_TAGS_TO_ROUTE : (T.F.id_tag, T.F.id_route),
        code.ADD_TAGS_TO_USER : (T.F.id_tag, T.F.id_user),
        code.UPD_OPERATOR : None,
        code.UPD_FB_USER : None,
        code.UPD_PRIZE_VALIDATION : None,
        code.UPD_ROUTE : None,
        code.UPD_PRIZE : None,
        code.UPD_OBJ : None,
        code.UPD_INFO_PROBABILITY : None,
        code.DEL_COMPL_PRIZE : None,
        code.DEL_OBJ_FROM_ROUTE : None,
        code.DEL_PRIZE_FROM_ROUTE : None,
        code.DEL_MAND_OBJ_FROM_INFO : None,
        code.DEL_MAND_OBJS_FROM_ROUTE : None,
        code.DEL_TAGS_FROM_ROUTE : None,
        code.DEL_TAGS_FROM_USER : None,
        code.DEL_ROUTE : None,
        code.DEL_PRIZE : None
    }

    def _get_query_and_attributes(self, code):
        """Returns the tuple (query, attributes) from the dicts using the
        supplied code.

        :param int code: query code used to fetch the query and attributes
        :return tuple(basestring, (basestring)): query to be executed and query returning attributes

        """
        try:
            query_to_be_executed = self._code_to_query[code]
            attributes = self._code_to_attributes[code]
            if isinstance(attributes, int):
                attributes = self._code_to_attributes[attributes]
        except KeyError:
            print "QUERY ERROR: invalid query code:", code
            raise Exception("invalid query code")
        return (query_to_be_executed, attributes)
    
    def _build_insert_query(self, attributes, n_insertions):
        """Returns the insert query in the right mysql syntax with all the
        needed placeholders.

        :param tuple(basestring) attributes: tuple containing the attributes names, used to build
            the part of the query, after the table name, i.e. (col1, ..., colN)
        :param int n_insertions: number of rows to insert in the table, used to
            append a corresponding number of (%s, ..., %s), where the number of %s
            is derived form the len(attributes)
        :return basestring: insert query to be performed

        """
        print "building insert query...",
        query = " ({0}) values ".format(", ".join(attributes))
        insertion = "({0})".format(", ".join(["%s"]*len(attributes)))
        query += ",".join([insertion]*n_insertions)
        print "done"
        return query

    def _build_insert_args(self, attributes, args):
        """Returns a list suitable to be the args parameter of the
        cursor.execute method for an insertion query.

        :param tuple(basestring) attributes: same as _build_insert_query, but used to fill the
            args list in the correct order
        :param list[dict] args: list containing dicts with keys corresponding to the
            attributes' names in the preceding argument and values corresponding to
            the attribute value of that element

        """
        print "building insert args...",
        built_args = []
        for element in args:
            for attribute in attributes:
                built_args.append(element[attribute])
        print "done"
        return built_args

    def execute(self, db, code, args=None, commit=True):
        """Returns a list of the row resulting from the query. The rows are
        turned into dictionary in the form attribute_name : value
        
        :param db: database cursor used to execute the query
        :param int code: query code
        :param dict | list[dict] | list[None] args: arguments dict to be supplied to the database cursor's
            execute method, relating to the variable parts of the query.
            In case of an insert query it's a dict of dicts, containing
            attribute:value pairs.
        :param bool commit: whether the function should issue a commit after the query
        :return list[dict] | int | long: query results | last insertion id | no rows affected
        
        """
        print "executing query..."
        query_to_be_executed, attributes = self._get_query_and_attributes(code)
        if not query_to_be_executed:
            return None
        is_select_query = query_to_be_executed.strip().startswith("select")
        is_insert_query = query_to_be_executed.startswith("insert")
        if is_insert_query:
            query_to_be_executed += self._build_insert_query(attributes, len(args))
            args = self._build_insert_args(attributes, args)
        elif args: # select, update or delete query
            to_format = False
            for key in args:
                if isinstance(args[key], list):
                    args[key] = ','.join('"'+str(subarg)+'"' for subarg in args[key])
                    print "FORMATTED", key, "TO:", args[key]
                    to_format = True
            if to_format:
                query_to_be_executed = query_to_be_executed.format(**args)
        print "ARGS:",args
        if args:
            form = args if isinstance(args, dict) else tuple(args)
            print "ESEGUO QUERY:",query_to_be_executed % form
        else:
            print "ESEGUO QUERY:",query_to_be_executed
        try:
            if args:
                db.execute(query_to_be_executed, args)
            else:
                db.execute(query_to_be_executed)
        except Exception as e:
            print "EXCEPTION QUERY:", repr(e)
            db.rollback()
            raise e
        if not is_select_query:
            if commit:
                db.commit()
            if is_insert_query: 
                return db.lastrowid
            else:
                return db.rowcount
        else:
            rows = db.fetchall()
            results = []
            for row in rows:
                dictFromRow = {}
                for i, attribute in enumerate(attributes):
                    dictFromRow[attribute] = row[i]
                results.append(dictFromRow)
            return results
