
# Table names class
class Tables:

    User = "User"
    Objective = "Objective"
    Route = "Route"
    Route_available = "Route_available"
    obj_route = "obj_route"
    Prize = "Prize"
    Prize_available = "Prize_available"
    Prize_info = "Prize_info"
    route_prize_info = "route_prize_info"
    Completion_info = "Completion_info"
    Mandatory_obj = "Mandatory_obj"
    Completion_obj = "Completion_obj"
    Completion_prize = "Completion_prize"
    Archive = "Archive"
    Tag = "Tag"
    tag_user = "tag_user"
    tag_route = "tag_route"

    # Field names class
    class F:
        # common fields
        id = "id"
        id_user = "id_user"
        id_operator = "id_operator"
        id_obj = "id_obj"
        id_prize = "id_prize"
        id_route = "id_route"
        id_tag = "id_tag"
        id_info = "id_info"
        name = "name"
        description = "description"
        position_latitude = "position_latitude"
        position_longitude = "position_longitude"
        deleted = "deleted"
        validation_code = "validation_code"
        validation_method = "validation_method"

        # User table
        login = "login"
        psw = "psw"
        salt = "salt"
        is_operator = "is_operator"
        name_operator = "name_operator"
        fb_id = "fb_id"

        # Objective table
        city = "city"

        # Route table
        validity_days = "validity_days"

        # Prize table
        visible = "visible"
        repeat_days = "repeat_days"

        # Completion_obj table
        date = "date"

        # Completion_prize table
        expiry_date = "expiry_date"
        validation_expiry_time = "validation_expiry_time"

        # Archive table
        completion_date = "completion_date"

        # Completion_info table
        probability = "probability"
