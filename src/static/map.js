var map;
var centreMarker;
var directionService;
var directionsDisplay;

function MarkerPlacer ()
{
	this.oldMarker = null
	
	this.placeMarker = function (position, map)
	{
		var newMarker = new google.maps.Marker({
			position: position,
			map: map
		});
		if (this.oldMarker) 
		{
				this.oldMarker.setMap(null);
				delete this.oldMarker;
		}
		this.oldMarker = newMarker;
		map.panTo(position);
	}
}

function MarkerPlacerDirections () {
	this.placer = new MarkerPlacer();
	
	this.placeMarker = function (position, map) {
		this.placer.placeMarker(position, map);
		var request = {
			origin: centreMarker.position,
			destination: this.placer.oldMarker.position,
			travelMode: google.maps.TravelMode.DRIVING
		};
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
				var sec = response.routes[0].legs[0].duration.value;
				document.getElementById('durata').innerHTML = sec + " secondi cioe' " +  sec/60 + " minuti" ;
			}
		});
	}
}

function initialize() 
{
	var mapOptions = {
		center: { lat: -34.488792130153335, lng: 150.88412761688232 },
		zoom: 12
	};
	
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	centreMarker = new google.maps.Marker({
		position: map.getCenter(),
		map: map
	});
	directionsService = new google.maps.DirectionsService();
	directionsDisplay = new google.maps.DirectionsRenderer({map: map});
	markerPlacer = new MarkerPlacerDirections();
	
	google.maps.event.addListener(map, 'click', function(e) {
		markerPlacer.placeMarker(e.latLng, map);
	});
}

google.maps.event.addDomListener(window, 'load', initialize);
