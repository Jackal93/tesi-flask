# -*- coding: utf-8 -*-

# TODO win prizes only in routes with no unclaimed won prizes

from contextlib import closing
from datetime import datetime, timedelta
import hashlib
import binascii
import httplib
import os
from random import random
from bisect import bisect
from functools import wraps

from flask import request, session, g, redirect, url_for, render_template, jsonify, make_response

import MySQLdb
from src import app
from src.query import Query
from src.tables import Tables as T
from src.jsonkeys import Keys as JK


# configuration
DBHOST = 'localhost'
DBNAME = 'my1504'
DBUSER = 'my1504'
DBPSW = 'movieghu'
OBJ_FORM = {T.F.name: basestring, T.F.description: basestring,
            T.F.city: basestring, T.F.position_latitude: float,
            T.F.position_longitude: float, T.F.validation_method: int}
PRIZE_FORM = {T.F.name: basestring, T.F.description: basestring, T.F.visible: bool,
              T.F.repeat_days: int,  T.F.validation_method: int}
OPERATOR_FORM = {T.F.name_operator: basestring,
                 T.F.position_latitude: float,
                 T.F.position_longitude: float}
CODE_EXPIRY_MINUTES = 5

app.config.from_object(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)
query = Query()


def connect_db():
    return MySQLdb.connect(DBHOST, DBUSER, DBPSW, DBNAME)


def init_db():
    """Creates the database tables."""
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()


def OK():
    """
    Returns a 200 OK empty response
    """
    resp = make_response()
    resp.status_code = httplib.OK
    return resp


def bad_request():
    """
    Returns a 400 BAD REQUEST empty response
    """
    resp = make_response()
    resp.status_code = httplib.BAD_REQUEST
    return resp


def bad_request_with_json(**kwargs):
    """
    Returns a 400 BAD REQUEST json response
    :param kwargs: argument passed to jsonify
    """
    resp = jsonify(**kwargs)
    resp.status_code = httplib.BAD_REQUEST
    return resp


def unauthorized():
    """
    Returns a 401 UNAUTHORIZED empty response
    """
    resp = make_response()
    resp.status_code = httplib.UNAUTHORIZED
    return resp


def forbidden():
    """
    Returns a 403 FORBIDDEN empty response
    """
    resp = make_response()
    resp.status_code = httplib.FORBIDDEN
    return resp


def conflict():
    """
    Returns a 409 CONFLICT empty response
    """
    resp = make_response()
    resp.status_code = httplib.CONFLICT
    return resp


def internal_server_error(**kwargs):
    """
    Returns a 500 INTERNAL SERVER ERROR json response
    :param kwargs: argument passed to jsonify
    """
    resp = jsonify(**kwargs)
    resp.status_code = httplib.INTERNAL_SERVER_ERROR
    return resp


@app.before_request
def before_request():
    """
    Attempts to connect to the db and logs info of received request
    """
    print datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    """Connects to the database for the new request."""
    try:
        dbconnection = connect_db()
        g.db = dbconnection.cursor()
        g.db.commit = lambda: dbconnection.commit()
        g.db.rollback = lambda: dbconnection.rollback()
        print "CONNESSIONE DB OK"
    except MySQLdb.MySQLError as e:
        print "EXCEPTION DB:", str(e)
        return internal_server_error(error="database unavailable")

    print "LOGGED IN AS:", session.get(T.F.id_user, None)
    print "URL:", request.url
    print "URL_RULE:", request.url_rule
    print "METHOD:", request.method
    print "USER-AGENT:", request.user_agent
    print "ACCEPT:", request.accept_mimetypes
    print "CONTENT-TYPE:", request.content_type
    if not request.method == "GET":
        if request.json:
            print "JSON:", request.json
    if request.form:
        print "FORM:", request.form


def guard(func):
    """
    Wrapper debug function used to print unhandled exceptions
    """
    @wraps(func)
    def func_wrapper(*args, **kwargs):
        try:
            response = func(*args, **kwargs)
            return response
        except Exception as e:
            print "GUARD:", repr(e)
            return internal_server_error(error=repr(e))
    return func_wrapper


@app.after_request
def after_request(response):
    print "RESPONSE STATUS CODE:", response.status_code
    return response


@app.teardown_request
def teardown_request(exception):
    """Close connection to the database after request was handled."""
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()
    print "\n------\n"


# works like isinstance but int and long are considered the same type
def islooseinstance(obj, pytype):
    if pytype == int or pytype == long:
        return isinstance(obj, int) or isinstance(obj, long)
    else:
        return isinstance(obj, pytype)


def is_well_formed(item, form):
    """Returns wether the supplied item is formed like the "form" dict. The
    function is recursive.
    
    :param item: the item to be checked. It must contain all keys in form
    and item[key] must be of type form[key]
    :param form: a dict key:type where type can be a python type, a dict (in
    which case is recursively checked) or a list with a single element that can
    be one of the preceding two. In the latter case, item[key] must be a list
    of objects with type form[key][0].
    
    """
    well_formed = True
    for key in form:
        if key not in item:
            print key, "was required"
            well_formed = False
            break
        if isinstance(form[key], type): # python type
            if not islooseinstance(item[key], form[key]):
                print key, "is", type(item[key]),"should be", form[key]
                well_formed = False
                break
        elif not isinstance(item[key], type(form[key])): # list or dict
            print key, "is", type(item[key]), "should be", type(form[key])
            well_formed = False
            break
        if isinstance(form[key], list): # list case
            type_ = form[key][0]
            if isinstance(type_, dict):
                check = lambda item, form: \
                        isinstance(item, dict) and is_well_formed(item, form)
            else:
                check = lambda item, pytype: islooseinstance(item, pytype)
            for subitem in item[key]:
                if not check(subitem, type_):
                    print "item in", key, "is", subitem,\
                        "should be", type_
                    well_formed = False
                    break
            if not well_formed:
                break
        elif isinstance(form[key], dict):
            if not is_well_formed(item[key], form[key]):
                print "...in item", key
                well_formed = False
                break
    return well_formed


@app.route('/')
@guard
def url_index():
    return render_template('index.html')


def get_tags(routes=[], users=[], in_dict=False):
    """
    Gets the tags of the supplied objectives, routes and users and optionally arranges them in {id: [tag]} dicts
        
    :param list[int] objs: list of objs id whose tags should be retrieved
    :param list[int] routes: list of routes id with the same purpose
    :param list[basestring] users: list of users id with the same purpose
    :param bool in_dict: whether the query result should be arranged in a dict or returned as-is
    :return dict: depending on inDict:
        - True: {'objs' : {id_obj : [tags]}, 'routes' : {id_route : [tags]}, 'users' : {id_user : [tags]}}
        - False: {'objs' : [{'id' : id_obj, 'id_tag' : tag}], 'routes' : [{'id' : id_route, 'id_tag' : tag}],
                    'users' : [{'id' : id_user, 'id_tag' : tag}]}
    """
    results = {}
    to_get_dict = {JK.routes: (routes, Query.code.GET_ROUTES_TAGS, Query.var.id_routes),
                   JK.users: (users, Query.code.GET_USERS_TAGS, Query.var.id_users)}
    for to_get in to_get_dict:
        id_list = to_get_dict[to_get][0]
        query_code = to_get_dict[to_get][1]
        key = to_get_dict[to_get][2]
        results[to_get] = []
        if len(id_list) > 0:
            query_res = query.execute(g.db, query_code, {key: id_list})
            if not in_dict:
                results[to_get] = query_res
            else:
                results[to_get] = {}
                for row in query_res:
                    results[to_get].setdefault(row[T.F.id], [])
                    results[to_get][row[T.F.id]].append(row[T.F.id_tag])
    return results

@app.route('/tags')
@guard
def url_get_tags():
    """
    Returns a json response with all the tags
    { 'tags': [tag1, tag2, ..., tagn] }
    """
    query_res = query.execute(g.db, Query.code.GET_TAGS)
    tags = [row[T.F.id_tag] for row in query_res]
    return jsonify(**{JK.tags: tags})

@app.route('/objs')
@guard
def url_get_objs():
    """
    Returns a json response with all the objectives
    { 'objs': [{obj1}, {obj2}, ..., {objn}] }
    """
    objs = query.execute(g.db, Query.code.GET_OBJS)
    return jsonify(**{JK.objs: objs})

@app.route('/routes/<float:lat>/<float:lng>/<int:distance>')
@guard
def url_get_routes_by_coord(lat, lng,  distance):
    """
    Returns a json response with all the routes and related objectives, prizes and operators within a certain radius
    from a geographical point
    :param lat: point latitude
    :param lng: point longitude
    :param distance: radius
    :return json: {'routes': [...], 'objs': [...], 'prizes': [...], 'operators': [...]}
    """
    routes = query.execute(g.db,
                           Query.code.GET_ROUTES_IN_RADIUS,
                           {T.F.position_latitude: lat, T.F.position_longitude: lng, Query.var.distance: distance})
    return jsonify(**(get_all_info_from_routes(routes)[0]))

@app.route('/routes', defaults = {'city' : None})
@app.route('/routes/<city>')
@guard
def url_get_routes_by_city(city):
    """
    Returns a json response with all the routes and related objectives, prizes and operators within a certain city, or
    all of them if no city is supplied.
    If the request is from an operator, then all of his routes are returned.
    :param city:
    :return:
    """
    code = Query.code.GET_ROUTES
    args = None
    if session.get(T.F.is_operator, False):
        id_operator = session.get(T.F.id_user, None)
        code = Query.code.GET_ROUTES_OF_OPERATOR
        args = {T.F.id_operator: id_operator}
    elif city is not None:
        code = Query.code.GET_ROUTES_IN_CITY
        args = {T.F.city: city}
    routes = query.execute(g.db, code, args)
    results, _ = get_all_info_from_routes(routes)
    if session.get(T.F.is_operator, False):
        all_objs = query.execute(g.db, Query.code.GET_OBJS)
        results[JK.objs] = all_objs
    return jsonify(**(results))

def get_all_info_from_routes(routes, get_operators=True):
    """
    Gets all routes, objectives, prizes and the respective operators out of the results of the query
    made with the supplied query code.

    :param list[dict] routes: results of a GET_ROUTES* query
    :return tuple(dict, dict): a tuple (results, parsed_prizes, objs), where:
        - results is a dict: {'routes' : [...], 'objs': [...], 'prizes' : [...], 'operators' : [...]}
        - parse_prizes: see parse_prizes_from_routes
    """
    objs = set()
    operators = set()
    routes_dict = {}
    results = {JK.routes: [], JK.objs: [], JK.prizes: [], JK.operators: []}
    parsed_prizes = []
    if len(routes) > 0:
        # parsing routes
        for row in routes:
            operator = row[T.F.id_operator]
            operators.add(operator)
            op_hash = hashlib.sha512(operator).hexdigest()
            route = {T.F.id: row[T.F.id_route], JK.objs: [],
                     T.F.name: row[T.F.name],
                     T.F.description: row[T.F.description],
                     T.F.validity_days: row[T.F.validity_days],
                     JK.prizes: [], JK.tags: [], T.F.id_operator: op_hash}
            routes_dict[row[T.F.id_route]] = route
            results[JK.routes].append(route)
        # getting and parsing the routes' objectives
        objs_query_res = query.execute(g.db, Query.code.GET_OBJS_FROM_ROUTES, {Query.var.id_routes: routes_dict.keys()})
        for row in objs_query_res:
            routes_dict[row[T.F.id_route]][JK.objs].append(row[T.F.id_obj])
            obj = row
            obj.pop(T.F.id_route)
            obj[T.F.id] = obj.pop(T.F.id_obj)
            if obj[T.F.id] not in objs:
                results[JK.objs].append(obj)
                objs.add(obj[T.F.id])
        # getting and parsing the routes' prizes
        query_res = query.execute(g.db,
                                  Query.code.GET_PRIZES_FROM_ROUTES,
                                  {Query.var.id_routes: routes_dict.keys()})
        parsed_prizes = parse_prizes_from_routes(query_res, routes_dict)
        results[JK.prizes] = parsed_prizes[JK.prizes]
        # getting routes' tags
        route_tags = get_tags(routes = routes_dict.keys())
        for tag in route_tags[JK.routes]:
            routes_dict[tag[T.F.id]][JK.tags].append(tag[T.F.id_tag])
        # getting operators if requested
        if get_operators:
            results[JK.operators] = get_operators_with_tags(operators)
    return results, parsed_prizes


def get_operators_with_tags(operators):
    """
    Gets the operators' info from the db whose ids are supplied

    :param set[int] operators: set of operators id whose info are to be fetched
    :return list[dict]: list of operators' info dicts
    """
    operators_dict = {}
    operators_list = []
    if len(operators) > 0:
        query_res = query.execute(g.db, Query.code.GET_OPERATORS_INFO, {Query.var.id_operators: list(operators)})
        for row in query_res:
            id_operator = row.pop(T.F.login)
            id_hashed = hashlib.sha512(id_operator).hexdigest()
            row[T.F.id] = id_hashed
            row[JK.tags] = []
            operators_dict[id_operator] = row
            operators_list.append(row)
        print "getting operators' tags..."
        tags_raw = get_tags(users = operators_dict.keys())['users']
        print "tags taken"
        for tag_dict in tags_raw:
            id_operator = tag_dict[T.F.id]
            tag = tag_dict[T.F.id_tag]
            operators_dict[id_operator][JK.tags].append(tag)
    return operators_list


def parse_prizes_from_routes(query_res, routes_to_prizes):
    """
    Parses the results of a GET_*PRIZES_FROM_ROUTES query and builds a dict with the extracted info.

    :param list[dict] query_res: the results of a GET_*PRIZES_FROM_ROUTES query
    :param dict routes_to_prizes: a dictionary with route ids as key and value a dict whose 'prizes' key is
        required to be already initialized with an empty list, that be filled with prizes id
    :return dict: {'prize_to_info' : {id_prize : {prize info}},
                    'prize_to_operator' : {id_prize : id_operator},
                    'prizes' : [prizes info],
                    'operators_id_set': set(operators' ids))}
    """
    is_client_operator = session.get(T.F.is_operator, False)
    route_to_prize_to_mand_objs = {}
    prize_to_info = {}
    prize_to_operator = {}
    prizes = []
    operators_id_set = set()
    for row in query_res:
        id_route = row[T.F.id_route]
        id_prize = row[T.F.id_prize]
        id_obj = row[T.F.id_obj]
        id_operator = row[T.F.id_operator]
        probability = row[T.F.probability]
        operators_id_set.add(id_operator)
        route_to_prize_to_mand_objs.setdefault(id_route, {})
        if id_prize not in route_to_prize_to_mand_objs[id_route]:
            prize_dict = {T.F.id: id_prize, JK.mandatory_objs: [], T.F.probability: probability}
            routes_to_prizes[id_route][JK.prizes].append(prize_dict)
            route_to_prize_to_mand_objs[id_route][id_prize] = prize_dict[JK.mandatory_objs]
        if id_prize not in prize_to_info:
            prize_to_info[id_prize] = {T.F.id: id_prize}
            for field in PRIZE_FORM:
                prize_to_info[id_prize][field] = row[field]
            id_op_hashed = hashlib.sha512(id_operator).hexdigest()
            prize_to_info[id_prize][T.F.id_operator] = id_op_hashed
            if not prize_to_info[id_prize][T.F.visible] and not is_client_operator:
                prize_to_info[id_prize][T.F.name] = ''
                prize_to_info[id_prize][T.F.description] = ''
            prize_to_operator[id_prize] = id_operator
            prizes.append(prize_to_info[id_prize])
        route_to_prize_to_mand_objs[id_route][id_prize].append(id_obj)
    return ({JK.prize_to_info: prize_to_info,
             JK.prize_to_operator: prize_to_operator,
             JK.prizes: prizes,
             JK.operators_id_set: operators_id_set})


def weighted_choice(choices):
    """
    Performs a random weigthed choice on the given choices
    :param list[tuple(object, int)] choices: list of tuples of 2 elements, the first being the choice and the second
        being its weight
    :return object: the selected choice
    """
    values, weights = zip(*choices)
    total = 0
    cum_weights = []
    for w in weights:
        total += w
        cum_weights.append(total)
    x = random() * total
    i = bisect(cum_weights, x)
    return values[i]


def validate_code_for_user(validation_code, id_user):
    """
    Checks if the supplied objective code is valid and if so register its completion for the given user

    :param basestring validation_code: objective validation code
    :param basestring id_user:
    :return int | None: the validated objective id, or None if the validation code was invalid
    """
    verification_results = query.execute(g.db,
                                         Query.code.GET_OBJ_FROM_VERIFICATION,
                                         {T.F.validation_code: validation_code})
    if len(verification_results) == 0:
        return None
    id_obj = verification_results[0][T.F.id]
    query.execute(g.db, Query.code.ADD_COMPLETE_OBJ,
                  [{T.F.id_obj: id_obj, T.F.id_user: id_user,
                    T.F.date: datetime.now().strftime("%Y-%m-%d %H:%M:%S")}])
    return id_obj


def assign_prizes_if_needed(id_user):
    """
    Checks if the given user won any prize and if so returns their info in a list.

    :param id_user:
    :return tuple(list[dict], dict): tuple <list of prizes won info, parsed_prizes (see parse_prizes_from_routes)>
    """
    user_routes_objs = query.execute(g.db,
                                     Query.code.GET_ROUTES_FROM_OBJS_OF_USER,
                                     {T.F.id_user: id_user})
    user_routes_to_objs = {} # id_route : [objs_verified]
    routes_to_prizes = {} # id_route : {'prizes' : [id_prizes]}
    # getting all objs the user completed in each route
    for row in user_routes_objs:
        user_routes_to_objs.setdefault(row[T.F.id_route], [])
        user_routes_to_objs[row[T.F.id_route]].append(row[T.F.id_obj])
        routes_to_prizes.setdefault(row[T.F.id_route], {JK.prizes: [], JK.tags: []})
    print "USER R:OBJ:", user_routes_to_objs
    routes_str = ', '.join(str(key) for key in user_routes_to_objs)
    print "ROUTES_STR:", routes_str
    # getting all prizes in routes of which the user completed any objective
    query_res = query.execute(g.db, Query.code.GET_NOT_WON_PRIZES_FROM_ROUTES,
                              {Query.var.id_routes: user_routes_to_objs.keys(), T.F.id_user: id_user})
    print "query res len:", len(query_res)
    # building routes_to_prizes
    parsed_prizes = parse_prizes_from_routes(query_res, routes_to_prizes)
    prize_to_info = parsed_prizes[JK.prize_to_info]
    # DBG
    dbg_rpi = {}
    for route in routes_to_prizes:
        dbg_rpi[route] = {}
        for prize_dict in routes_to_prizes[route][JK.prizes]:
            id_prize = prize_dict[T.F.id]
            dbg_rpi[route][id_prize] = {}
            dbg_rpi[route][id_prize]['m_objs'] = prize_dict[JK.mandatory_objs]
    print "R:P:I:", dbg_rpi
    # END_DBG
    # getting the prizes the user can win
    route_to_winnable_prizes = {}
    winnable_prizes_set = set()
    for id_route in routes_to_prizes:
        for prize_dict in routes_to_prizes[id_route][JK.prizes]:
            id_prize = prize_dict[T.F.id]
            if id_prize not in winnable_prizes_set:
                print "checking prize", id_prize, "in route", id_route
                completed = True
                mandatory_objs = prize_dict[JK.mandatory_objs]
                if len(mandatory_objs) > len(user_routes_to_objs[id_route]):
                    print "not enough objs completed, req:", mandatory_objs, \
                            "completed:", user_routes_to_objs[id_route]
                    completed = False
                    continue
                for mandatory_obj in mandatory_objs:
                    if (mandatory_obj != 0 and
                            mandatory_obj not in user_routes_to_objs[id_route]):
                        print "obj", mandatory_obj, "not completed"
                        completed = False
                        break
                if completed:
                    winnable_prizes_set.add(id_prize)
                    print "COMPLETED: ROUTE="+str(id_route)+" PRIZE="+str(id_prize)
                    winnable_prize = prize_to_info[id_prize]
                    weight = prize_dict[T.F.probability]
                    route_to_winnable_prizes.setdefault(id_route, [])
                    route_to_winnable_prizes[id_route].append((winnable_prize, weight))
    # DBG
    dbg_win = {}
    for id_route in route_to_winnable_prizes:
        dbg_win[id_route] = []
        for prize, weight in route_to_winnable_prizes[id_route]:
            dbg_win[id_route].append({T.F.id: prize[T.F.id], '%' : weight})
    print "R:WINNABLE:", dbg_win
    # END_DBG
    # assinging the user a random prize for each route in which he can win any
    prizes_won = []
    for id_route in route_to_winnable_prizes:
        prizes_won.append(weighted_choice(route_to_winnable_prizes[id_route]))
    query_args = []
    prize_expiry_date = (datetime.now() + timedelta(30)).strftime("%Y-%m-%d %H:%M:%S")
    # DBG
    dbg_won = []
    for prize in prizes_won:
        dbg_won.append(prize[T.F.id])
        query_args.append(
            {T.F.id_prize: prize[T.F.id], T.F.id_user: id_user,
             T.F.expiry_date: prize_expiry_date})
    print "WON:", dbg_won
    # END_DBG
    # registering won prizes in the db
    if len(prizes_won) > 0:
        query.execute(g.db, Query.code.ADD_COMPLETE_PRIZE, query_args)
    print "DONE WITH PRIZES"
    return prizes_won, parsed_prizes


@app.route('/obj/verify', methods=['POST'])
@guard
def url_verify():
    """
    Verify if the supplied validation code in the json is valid, and if so register the completion of the related
    objective to the user.
    Returns the id of the objective and all the routes that pass through that objective (as a /routes request)
    """
    id_user = session.get(T.F.id_user, None)
    if id_user is None:
        return unauthorized()
    form = {T.F.validation_code: basestring}
    if not is_well_formed(request.json, form):
        return bad_request()
    validation_code = request.json[T.F.validation_code]
    id_obj = validate_code_for_user(validation_code, id_user)
    if id_obj is None:
        return bad_request()
    prizes_won, parsed_prizes = assign_prizes_if_needed(id_user)
    routes = query.execute(g.db, Query.code.GET_ROUTES_FROM_OBJ, {T.F.id_obj: id_obj})
    response, response_parsed_prizes = get_all_info_from_routes(routes, get_operators=False)
    routes_operators = response_parsed_prizes['operators_id_set']
    won_prizes_operators = set()
    for prize in prizes_won:
        won_prizes_operators.add(parsed_prizes[JK.prize_to_operator][prize[T.F.id]])
    all_operators = won_prizes_operators.union(routes_operators)
    response[JK.operators] = get_operators_with_tags(all_operators)
    response[JK.prizes_won] = prizes_won
    response[JK.verified_obj] = id_obj
    return jsonify(**response)

@app.route('/objs/add', methods=['POST'])
@guard
def url_add_objs():
    """
    Adds new objectives whose info are supplied in the request json.
    Returns the ids of the new objectives.
    :return json: {'objs': [id1, id2, ..., idn]}
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    form = {JK.objs: [OBJ_FORM]}
    if not is_well_formed(request.json, form):
        return bad_request()
    objs = request.json[JK.objs]
    added_objs = add_new_objs(objs)
    return jsonify(**{JK.objs: added_objs})

def add_new_objs(objs, commit=True):
    """
    Adds the supplied objectives to the db and commits unless commit is set to False
    :param list[dict] objs: list of objectives info
    :param bool commit: whether to commit
    :return list[int]: added objs ids list
    """
    val_codes = []
    query_args = []
    for obj in objs:
        safe_obj = OBJ_FORM.copy()
        for field in safe_obj:
            safe_obj[field] = obj[field]
        code_to_hash = safe_obj[T.F.name] + os.urandom(16)
        val_code = hashlib.sha512(code_to_hash).hexdigest()
        val_codes.append(val_code)
        safe_obj[T.F.validation_code] = val_code
        query_args.append(safe_obj)
    first_id = query.execute(g.db, Query.code.ADD_OBJ, query_args, commit)
    added_objs = []
    for i, val_code in enumerate(val_codes):
        added_objs.append({T.F.id: i+first_id,
                                T.F.validation_code: val_code})
    return added_objs

@app.route('/route/add', methods=['POST'])
@guard
def url_add_route():
    """
    Adds a new route whose info are supplied in the request json.
    Optionally adds existing or new objs to the route.
    Optionally adds tags to the route.
    :return json: {'id_route': new route id, 'objs': [new objs ids]}
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    id_operator = session.get(T.F.id_user, None)
    form = {T.F.name: basestring, T.F.description: basestring,
            T.F.validity_days: int}
    if not is_well_formed(request.json, form):
        return bad_request()
    print "checks ok"
    request.json[T.F.id_operator] = id_operator
    id_route = query.execute(g.db, Query.code.ADD_ROUTE, 
                             [request.json], commit=False)
    print "route",id_route,"inserted"
    success, added_objs = edit_route_objs(id_route, request.json)
    if not success:
        return bad_request()
    if not edit_route_tags(id_route, request.json):
        return bad_request()
    g.db.commit()
    response = {T.F.id_route: id_route, JK.objs: added_objs}
    return jsonify(**response)

def edit_route_objs(id_route, objs_dict):
    """
    Creates the objectives in the 'new_objs' fields and adds them to the route.
    Adds the objectives in the 'id_objs_to_add' field to the route.
    Removes the objectives in the 'id_objs_to_remove' field from the route.
    Does not commit anything.

    :param int id_route:
    :param dict objs_dict:
    :return tuple(bool, list[int]): tuple <whether everything was successful, added objs id list>
    """
    failure = (False, [])
    added_objs = []
    # adding the new objs (if supplied) to the db
    if JK.new_objs in objs_dict:
        if not is_well_formed(objs_dict, {JK.new_objs: [OBJ_FORM]}):
            return failure
        print "adding new objs..."
        added_objs = add_new_objs(objs_dict[JK.new_objs], commit=False)
        # adding the new objs to the existing ones to be added to the route
        objs_dict.setdefault(JK.id_objs_to_add, [])
        for obj in added_objs:
            objs_dict[JK.id_objs_to_add].append(int(obj[T.F.id]))
        print "done adding new objs"
    # adding the supplied existing objs to the route
    if JK.id_objs_to_add in objs_dict:
        if not is_well_formed(objs_dict, {JK.id_objs_to_add: [int]}):
            return failure
        print "adding objs to route..."
        if not add_objs_to_route(id_route, objs_dict[JK.id_objs_to_add], commit=False):
            return failure
        print "objs added to route"
    if JK.id_objs_to_remove in objs_dict:
        if not is_well_formed(objs_dict, {JK.id_objs_to_remove: [int]}):
            return failure
        print "removing objs from route...",
        successful = remove_objs_from_route(id_route, objs_dict[JK.id_objs_to_remove], commit=False)
        if not successful:
            return failure
        # removing these objs as mandatory objs for all the route prizes
        query.execute(g.db, Query.code.DEL_MAND_OBJS_FROM_ROUTE, {T.F.id_route: id_route}, commit=False)
        print "done removing objs from route"
    return (True, added_objs)


def add_objs_to_route(id_route, objs, commit=True):
    """
    Returns whether the objectives were successfully added to the route
    
    :param int id_route: id of the route the objs should be added to
    :param list[int] objs: list of objs id that should be added to the route
    :return bool: whether adding was successful
    """
    if len(objs) == 0:
        return True
    query_args = []
    for obj in objs:
        query_args.append({T.F.id_route: id_route, T.F.id_obj: obj})
    try:
        query.execute(g.db, Query.code.ADD_OBJ_TO_ROUTE, query_args, commit)
    except MySQLdb.IntegrityError:
        return False
    return True


def remove_objs_from_route(id_route, objs, commit=True):
    """
    Returns whether the objectives were successfully added to the route

    Same params as add_objs_to_route
    :return bool: whether removing was successful
    """
    if len(objs) == 0:
        return True
    query_args = {T.F.id_route: id_route, Query.var.id_objs: objs}
    try:
        query.execute(g.db, Query.code.DEL_OBJ_FROM_ROUTE, query_args, commit)
    except MySQLdb.IntegrityError:
        return False
    return True

@app.route('/route/add/prize', methods=['POST'])
@guard
def url_add_existing_prize_to_route():
    """
    Adds an existing prize whose id is in the json to the route corresponding with the supplied id with the given
    probability
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    form = {T.F.id_route: int, T.F.id_prize: int, T.F.probability: int}
    if not is_well_formed(request.json, form):
        return bad_request()
    id_operator = session.get(T.F.id_user, None)
    id_prize = request.json[T.F.id_prize]
    id_route = request.json[T.F.id_route]
    if not route_belongs_to_operator(id_route, id_operator) or not prize_belongs_to_operator(id_prize, id_operator):
        return forbidden()
    id_info = add_prize_to_route(id_route, id_prize)
    if id_info is None:
        return bad_request()
    if not edit_prize_mand_objs_and_probability(id_info, request.json, force_probability=True):
        return bad_request()
    g.db.commit()
    return OK()


def add_prize_to_route(id_route, id_prize):
    """
    Adds a new completion_info and adds the supplied prize to the route.
    Returns the id of the completion_info.
    May raise MySQLdb.IntegrityError if either route or prize don't exist.
    Doesn't commit changes to the database
    
    :param int probability: Completion_info probability
    :param int id_route: id of the route the prize should be added to
    :param int id_prize: the id of the prize to be added to the route
    :return int | None: id_info of the Completion_info added or None if either id_route or id_prize are incorrect
    """
    id_info = query.execute(g.db, Query.code.ADD_COMPL_INFO, [None], commit=False)
    try:
        query.execute(g.db, Query.code.ADD_ROUTE_PRIZE_INFO,
                      [{T.F.id_route: id_route, T.F.id_prize: id_prize,
                      T.F.id_info: id_info}], commit=False)
    except MySQLdb.IntegrityError:
        return None
    return id_info


@app.route('/route/delete', methods=['DELETE'])
@guard
def url_delete_route():
    """
    Performs a logical delete of the supplied route in the json
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    id_operator = session.get(T.F.id_user, None)
    form = {T.F.id: [int]}
    if not is_well_formed(request.json, form):
        return bad_request()
    routes = request.json[T.F.id]
    num_rows = query.execute(g.db, Query.code.DEL_ROUTE, {Query.var.id_routes: routes, T.F.id_operator: id_operator})
    if num_rows == 0:
        return bad_request()
    return OK()

@app.route('/prize/add', methods=['POST'])
@guard
def url_add_prize():
    """
    Adds a new prize whose info are supplied in the json to an existing route with a probability
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    form = {T.F.id_route: int, T.F.probability: int}
    if not is_well_formed(request.json, form) or not is_well_formed(request.json, PRIZE_FORM):
        return bad_request()
    print "checks ok"
    id_operator = session.get(T.F.id_user, None)
    id_route = request.json[T.F.id_route]
    if not route_belongs_to_operator(id_route, id_operator):
        return forbidden()
    request.json[T.F.id_operator] = id_operator
    id_prize = query.execute(g.db, Query.code.ADD_PRIZE, [request.json], commit=False)
    id_info = add_prize_to_route(id_route, id_prize)
    if id_info is None:
        return bad_request()
    if not edit_prize_mand_objs_and_probability(id_info, request.json, force_probability=True):
        return bad_request()
    g.db.commit()
    return jsonify(**{T.F.id_prize: id_prize})

def add_mand_obj_to_info(mandatory_objs, id_info, commit=True):
    """Adds the objs whose id is supplied to the Completion_info related to the
    supplied id. Raises an exception if the objs or the info don't exist.
    
    :param list[int] mandatory_objs: list of obj id
    :param int id_info: id of the completion_info they objs are to be added to
    :return None:
    """
    if len(mandatory_objs) == 0:
        return True
    query_args = []
    for obj in mandatory_objs:
        query_args.append({T.F.id_info: id_info, T.F.id_obj: obj})
    try:
        query.execute(g.db, Query.code.ADD_MANDATORY_OBJS_TO_INFO, query_args, commit)
    except MySQLdb.IntegrityError:
        return False
    return True

def rem_mand_obj_from_info(mandatory_objs, id_info, commit=True):
    """Remove the supplied mandatory objs to the completion_info
    
    :param list[int] mandatory_objs: list of obj id
    :param int id_info: id of the completion_info they objs are to be added to
    :return None:
    """
    if len(mandatory_objs) == 0:
        return True
    try:
        query.execute(g.db, Query.code.DEL_MAND_OBJ_FROM_INFO,
                      {Query.var.id_objs: mandatory_objs, T.F.id_info: id_info}, commit)
    except MySQLdb.IntegrityError:
        return False
    return True

def edit_prize_mand_objs_and_probability(id_info, prize_dict, force_probability=False):
    """
    Sets the probability of the Completion_info with the given id. If force_probability is True and prize_dict doesn't
    contain the key 'probability' then nothing is done and False is returned.
    Adds the mandatory objs in the 'mand_objs_to_add' field to and removes those in the 'mand_objs_to_remove' field from
    the Mandatory_obj releated to the given id info.
    Doesn't commit anything.

    :param int id_info:
    :param dict prize_dict:
    :param bool force_probability:
    :return bool: whether everything was successful
    """

    if is_well_formed(prize_dict, {T.F.probability: int}):
        probability = prize_dict[T.F.probability]
        query.execute(g.db, Query.code.UPD_INFO_PROBABILITY, {T.F.probability: probability, T.F.id_info: id_info},
                      commit=False)
    elif force_probability:
        return False
    if JK.mand_objs_to_add in prize_dict:
        if not is_well_formed(prize_dict, {JK.mand_objs_to_add: [int]}):
            return False
        if not add_mand_obj_to_info(prize_dict[JK.mand_objs_to_add], id_info, commit=False):
            return False
    if JK.mand_objs_to_remove in prize_dict:
        if not is_well_formed(prize_dict, {JK.mand_objs_to_remove: [int]}):
            return False
        if not rem_mand_obj_from_info(prize_dict[JK.mand_objs_to_remove], id_info, commit=False):
            return False
    return True

def get_info_from_route_prize(id_route, id_prize, id_operator):
    """Returns id_info related to route and prize supplied belonging to the
    logged operator. If route or prize are wrong or don't belong to the
    operator, then False is returned
    
    :param int id_route: route id
    :param int id_prize: prize id
    :param int id_operator: logged in operator
    :return int | None: id_info or None if parameters are incorrect
    """
    query_res = query.execute(g.db,
                              Query.code.GET_INFO_FROM_ROUTE_PRIZE_OF_OPERATOR,
                              {T.F.id_route: id_route, T.F.id_prize: id_prize, T.F.id_operator: id_operator})
    if len(query_res) == 0:
        print "id_info not found"
        return None
    return query_res[0][T.F.id_info]


@app.route('/prize/collect', methods=['POST'])
@guard
def url_prize_collect():
    """
    Returns the the validation code of the supplied prize in the json that the logged in user won, if the prize was
    really won
    """
    id_user = session.get(T.F.id_user, None)
    if id_user is None:
        return unauthorized()
    form = {T.F.id_prize: int}
    if not is_well_formed(request.json, form):
        return bad_request()
    id_prize = request.json[T.F.id_prize]
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    to_hash = str(id_prize) + id_user + os.urandom(16)
    val_code = hashlib.sha512(to_hash).hexdigest()
    num_rows = query.execute(g.db, Query.code.UPD_PRIZE_VALIDATION,
                             {T.F.validation_expiry_time: now, T.F.validation_code: val_code, T.F.id_prize: id_prize,
                              T.F.id_user: id_user})
    if num_rows==0:
        return forbidden()
    return jsonify(**{T.F.validation_code: val_code})


@app.route('/prize/verify', methods=['POST'])
@guard
def url_prize_verify():
    """
    Returns the prize info of the supplied prize validation code in the json, if the code is correct.
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    id_operator = session.get(T.F.id_user, None)
    form = {T.F.validation_code: basestring}
    if not is_well_formed(request.json, form):
        return bad_request()
    val_code = request.json[T.F.validation_code]
    query_res = query.execute(g.db, Query.code.GET_COMPL_PRIZE_BY_CODE,
                              {T.F.validation_code: val_code, Query.var.val_minutes: CODE_EXPIRY_MINUTES})
    code_correct = (len(query_res) > 0)
    if not code_correct:
        print "validation code not correct"
        query_res = query.execute(g.db,
                                  Query.code.GET_WON_PRIZE_DATE_BY_VAL_CODE,
                                  {T.F.validation_code: val_code})
        if len(query_res) > 0:
            completion_date = query_res[0][T.F.completion_date]
            print "last date:", completion_date
            return bad_request_with_json(**{JK.last_won_date: str(completion_date)})
        else:
            return bad_request()
    prize_info = query_res[0]
    if prize_info[T.F.id_operator] != id_operator:
        print "prize not belonging to the operator"
        return forbidden()
    archive_fields = [T.F.id_prize, T.F.id_user]
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    archive_info = {T.F.completion_date: now, T.F.validation_code: val_code}
    for field in archive_fields:
        archive_info[field] = prize_info[field]
    query.execute(g.db, Query.code.ADD_ARCHIVE_PRIZE, [archive_info],
                  commit=False)
    query.execute(g.db, Query.code.DEL_COMPL_PRIZE,
                  {T.F.id_prize: prize_info[T.F.id_prize], T.F.id_user: prize_info[T.F.id_user]})
    prize_info.pop(T.F.id_operator)
    prize_info[T.F.id] = prize_info.pop(T.F.id_prize)
    prize_info.pop(T.F.id_user)
    return jsonify(**prize_info)


@app.route('/route/remove/prize', methods=['DELETE'])
@guard
def url_remove_prize_from_route():
    """Removes a prize from a route
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    form = {T.F.id_prize : int, T.F.id_route: int}
    if not is_well_formed(request.json, form):
        return bad_request()
    id_operator = session.get(T.F.id_user, None)
    id_prize = request.json[T.F.id_prize]
    id_route = request.json[T.F.id_route]
    if not remove_prize_from_route(id_prize, id_route, id_operator):
        return bad_request()
    return OK()


def remove_prize_from_route(id_prize, id_route, id_operator, commit=True):
    """
    Performs the query to remove a prize from a route
    
    :param int id_prize: prize id
    :param int id_route: route id
    :param basestring id_operator: operator login
    :return bool: whether removing was successful
    """
    num_rows = query.execute(g.db, Query.code.DEL_PRIZE_FROM_ROUTE,
                             {T.F.id_prize: id_prize, T.F.id_route: id_route, T.F.id_operator: id_operator},
                             commit)
    if num_rows == 0:
        return False
    return True


def route_belongs_to_operator(id_route, id_operator):
    """
    Checks if the given route belongs to the operator
    :param id_route:
    :param id_operator:
    :return bool: whether or not the route belongs to the operator
    """
    route_operator_res = query.execute(g.db, Query.code.GET_ROUTE_OPERATOR, {T.F.id_route: id_route})
    if len(route_operator_res) == 0:
        return False
    route_operator = route_operator_res[0][T.F.id_operator]
    return route_operator == id_operator


def prize_belongs_to_operator(id_prize, id_operator):
    """
    Checks if the given prize belongs to the operator
    :param id_prize:
    :param id_operator:
    :return bool: whether or not the prize belongs to the operator
    """
    query_res = query.execute(g.db, Query.code.GET_PRIZE_OPERATOR, {T.F.id_route: id_prize})
    if len(query_res) == 0:
        return False
    prize_operator = query_res[0][T.F.id_operator]
    return prize_operator == id_operator


@app.route('/route/edit', methods=['PUT'])
@guard
def url_edit_route():
    """
    Edits an existing route with the info supplied in the json.
    Optionally adds or removes objs.
    Optionally adds or remove tags.
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    form = {T.F.id: int, T.F.name: basestring, T.F.description: basestring,
            T.F.validity_days: int}
    if not is_well_formed(request.json, form):
        return bad_request()
    id_route = request.json[T.F.id]
    name = request.json[T.F.name]
    desc = request.json[T.F.description]
    val_days = request.json[T.F.validity_days]
    if not route_belongs_to_operator(id_route, session.get(T.F.id_user)):
        return unauthorized()
    query.execute(g.db, Query.code.UPD_ROUTE,
                  {T.F.name: name, T.F.description: desc, T.F.validity_days: val_days, T.F.id_route: id_route},
                  commit=False)
    if not edit_route_tags(id_route, request.json):
        return bad_request()
    success, added_objs = edit_route_objs(id_route, request.json)
    if not success:
        return bad_request()
    response = jsonify(**{JK.objs: added_objs})
    print "committing...",
    g.db.commit()
    return response


def edit_route_tags(id_route, tags_dict):
    """
    Adds and removes from the route corresponding the supplied id the tags found, respectively, in
    tags_dict[JK.tags_to_add] and tags_dict[JK.tags_to_remove]
    Doesn't commit anything.

    :param int id_route: id of the route the tags are to be added and/or removed to/from
    :param dict tags_dict: dict containing the tags
    :return bool: whether adding and removing were successful
    """
    return edit_tags_of(T.F.id_route, id_route, tags_dict)


def edit_user_tags(id_user, tags_dict):
    """
    Adds and removes the given tags to/from the given user, found respectively in: tags_dict[JK.tags_to_add] and
    tags_dict[JK.tags_to_remove]
    Doesn't commit anything.

    :param int id_user: id of the user the tags are to be added and/or removed to/from
    :param dict tags_dict: dict containing the tags
    :return bool: whether adding and removing were successful
    """
    return edit_tags_of(T.F.id_user, id_user, tags_dict)


def edit_tags_of(of_what, id_thing, tags_dict):
    """
    Edits the tags of either a route or a user, depending on of_what value.
    Doesn't commit anything.

    :param of_what: either T.F.id_route or T.F.id_user
    :param id_thing: route or user id
    :param tags_dict: dicts containing the tags to be added and removed, respectively in the keys JK.tags_to_add and
        JK.tags_to_remove
    :return:
    """
    if JK.tags_to_add in tags_dict:
        if not is_well_formed(tags_dict, {JK.tags_to_add: [basestring]}):
            return False
        tags_to_add = tags_dict[JK.tags_to_add]
        print "tags to add:", tags_to_add
        if not add_tags_to(of_what, id_thing, tags_to_add, commit=False):
            return False
    if JK.tags_to_remove in tags_dict:
        if not is_well_formed(tags_dict, {JK.tags_to_remove: [basestring]}):
            return False
        tags_to_remove = tags_dict[JK.tags_to_remove]
        print "tags to rem:", tags_to_remove
        if not remove_tags_from(of_what, id_thing, tags_to_remove, commit=False):
            return False
    return True


def add_tags_to(to_what, id_thing, tags, commit=True):
    """
    Adds tags to a route or a user, depending on to_what value
    :param basestring to_what: either JK.id_route or JK.id_user
    :param int id_thing: route or user id
    :param list[basestring] tags: list of tags to be added
    :param bool commit: whether to commit
    :return bool: whether tags were added successfully
    """
    if len(tags) == 0:
        return True
    query_args = []
    for tag in tags:
        query_args.append({T.F.id_tag: tag, to_what: id_thing})
    query_code = Query.code.ADD_TAGS_TO_ROUTE if to_what == T.F.id_route else Query.code.ADD_TAGS_TO_USER
    try:
        query.execute(g.db, query_code, query_args, commit)
    except MySQLdb.IntegrityError:
        return False
    return True


def remove_tags_from(from_what, id_thing, tags, commit=True):
    """
    Removes tags from a route or a user, depending on from_what value
    :param basestring from_what: either JK.id_route or JK.id_user
    :param int id_thing: route or user id
    :param list[basestring] tags: list of tags to be removed
    :param bool commit: whether to commit
    :return bool: whether tags were removed successfully
    """
    if len(tags) == 0:
        return True
    query_code = Query.code.DEL_TAGS_FROM_ROUTE if from_what == T.F.id_route else Query.code.DEL_TAGS_FROM_USER
    try:
        query.execute(g.db, query_code, {from_what: id_thing, Query.var.id_tags: tags}, commit)
    except MySQLdb.IntegrityError:
        return False
    return True


@app.route('/prize/edit', methods=['PUT'])
@guard
def url_edit_prize():
    """
    Edits an existing route with the info supplied in the json.
    Optionally adds or removes objs.
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    id_operator = session.get(T.F.id_user, None)
    form = {T.F.id: int}
    if not is_well_formed(request.json, form) or not is_well_formed(request.json, PRIZE_FORM):
        return bad_request()
    id_prize = request.json[T.F.id]
    name = request.json[T.F.name]
    desc = request.json[T.F.description]
    visible = request.json[T.F.visible]
    rep_days = request.json[T.F.repeat_days]
    val_method = request.json[T.F.validation_method]
    if not prize_belongs_to_operator(id_prize, id_operator):
        return forbidden()
    query.execute(g.db, Query.code.UPD_PRIZE,
                  {T.F.name: name, T.F.description: desc, T.F.visible: visible, T.F.repeat_days: rep_days,
                   T.F.id_prize: id_prize, T.F.id_operator: id_operator}, commit=False)
    if is_well_formed(request.json, {T.F.id_route: int}):
        id_route = request.json[T.F.id_route]
        if not route_belongs_to_operator(id_route, id_operator):
            return forbidden()
        id_info = get_info_from_route_prize(id_route, id_prize, id_operator)
        if id_info is None:
            return bad_request()
        if not edit_prize_mand_objs_and_probability(id_info, request.json):
            return bad_request()
    g.db.commit()
    return OK()


@app.route('/prize/delete', methods=['DELETE'])
@guard
def url_delete_prize():
    """
    Performs a logical delete of the supplied prize in the json
    """
    if not session.get(T.F.is_operator, False):
        return unauthorized()
    id_operator = session.get(T.F.id_user, None)
    form = {T.F.id: [int]}
    if not is_well_formed(request.json, form):
        return bad_request()
    prizes = request.json[T.F.id]
    num_rows = query.execute(g.db, Query.code.DEL_PRIZE, {Query.var.id_prizes: prizes, T.F.id_operator: id_operator})
    if num_rows == 0:
        return bad_request()
    return OK()


@app.route('/signup', methods=['POST'])
@guard
def url_signup():
    """
    Registers a new user, whose info are supplied in the json.
    If is_operator is set to True, then the user is set as an operator.
    The client is considered as logged in if the registration is successful.
    """
    user_info = request.json
    print "USER_INFO:", user_info
    user_form = {T.F.login: basestring, T.F.psw: basestring,
                 T.F.is_operator: bool}
    # optionally supplied tags in JK.tags
    if not is_well_formed(user_info, user_form):
        return bad_request()
    if user_info[T.F.is_operator]:
        if not is_well_formed(user_info, OPERATOR_FORM):
            return bad_request()
    print "CHEKS OK"
    query_res = query.execute(g.db, Query.code.GET_USER_BY_LOGIN, {T.F.login: user_info[T.F.login]})
    name_already_exists = (len(query_res) > 0)
    if not name_already_exists:
        print "USERNAME OK"
        if not add_new_user(user_info):
            return bad_request()
        set_session(user_info)
    else:
        print "USERNAME ALREADY USED"
        return conflict()
    return OK()


def add_new_user(user_info):
    """
    Adds a new user to the db with the supplied info
    :param dict user_info: key 'salt' is added and 'psw' modified
    :return bool: whether adding the user was successful
    """
    # setting operators filed to None if they were not supplied
    for field in OPERATOR_FORM:
        user_info.setdefault(field, None)
    salt = binascii.hexlify(os.urandom(8))
    user_info[T.F.salt] = salt
    user_info[T.F.psw] = hashlib.sha512(user_info[T.F.psw] + salt).hexdigest()
    query.execute(g.db, Query.code.ADD_USER, [user_info], commit=False)
    user_info[JK.tags_to_add] = user_info.pop(JK.tags, [])
    if not edit_user_tags(user_info[T.F.login], user_info):
        return False
    g.db.commit()
    return True


@app.route('/login', methods=['POST'])
@guard
def url_login():
    """
    Logs in the user whose login and password are supplied.
    """
    form = {T.F.login: basestring, T.F.psw: basestring}
    if not is_well_formed(request.json, form):
        return bad_request()
    username = request.json[T.F.login]
    password = request.json[T.F.psw]
    user_info = authenticate(username, password)
    print "after authenticate"
    if user_info is None:
        return unauthorized()
    set_session(user_info)
    print "after set session"
    prepare_user_info_for_return(user_info)
    print "after preparing"
    return jsonify(**user_info)

def authenticate(username, password):
    """ Authenticates the user whose info are provided.
    Returns a tuple (user_info, error): 
        - if the authentication succeeds user_info is set and error is None
        - otherwise user_info is None and error is set with the error message
    :param basestring username:
    :param basetring password:
    :return tuple(dict, basestring): tuple <user info, error string>
    """
    results = query.execute(g.db, Query.code.GET_USER_BY_LOGIN, {T.F.login: username})
    name_ok = False
    psw_ok = False
    user_info = None
    if results:
        user_info = results[0]
        name_ok = True
        password = hashlib.sha512(password + user_info[T.F.salt]).hexdigest()
        print 'USER:', user_info[T.F.login], "attempts login...",
        if user_info[T.F.psw] == password:
            psw_ok = True
    if not name_ok:
        print "wrong username"
        user_info = None
    elif not psw_ok:
        print "wrong psw"
        user_info = None
    else:
        print "login succesful"
    return user_info

def prepare_user_info_for_return(user_info):
    """
    Removes field from the user_info that shouldn't be publicly displayed
    :param user_info: dict with the user info
    :return: None
    """
    username = user_info[T.F.login]
    tags = get_tags(users=[username], in_dict=True)['users'].get(username, [])
    user_info[JK.tags] = tags
    user_info.pop(T.F.login, None)
    user_info.pop(T.F.psw, None)
    user_info.pop(T.F.salt, None)
    user_info.pop(T.F.fb_id, None)


@app.route('/loginfb', methods=['POST'])
@guard
def url_login_fb():
    """
    Logs in the user whose facebook id is supplied.
    If the user doesn't exist, it is created.
    """
    form = {T.F.fb_id : basestring}
    if not is_well_formed(request.json, form):
        return bad_request()
    fb_id = request.json[T.F.fb_id]
    hashed_id = hashlib.sha512(fb_id).hexdigest()
    query_res = query.execute(g.db, Query.code.GET_USER_BY_FB, {T.F.fb_id: hashed_id})
    user_info = {}
    if len(query_res) == 0:
        print "new fb user"
        # check if login and psw were supplied, if so the client wants to connect that account to fb
        if is_well_formed(request.json, {T.F.login : basestring, T.F.psw: basestring}):
            login = request.json[T.F.login]
            psw = request.json[T.F.psw]
            print "wants to connect account:", login
            user_info = authenticate(login, psw)
            if user_info is None:
                return unauthorized()
        else:
            print "doesn't want to connect account"
            login = hashed_id
            user_info = {T.F.login: hashed_id, T.F.fb_id: hashed_id,
                         T.F.psw: binascii.hexlify(os.urandom(8)), JK.new_user: True}
            is_operator = False
            form = {T.F.is_operator: bool}
            if is_well_formed(request.json, form):
                is_operator = request.json[T.F.is_operator]
            user_info[T.F.is_operator] = is_operator
            if is_operator and is_well_formed(request.json, OPERATOR_FORM):
                for key in form:
                    user_info[key] = request.json[key]
            if not add_new_user(user_info):
                return bad_request()
        if not connect_fb(hashed_id, login):
            return conflict()
    else:
        print "existing fb user"
        user_info = query_res[0]
        user_info[JK.new_user] = False
    print "user_info:", user_info
    set_session(user_info)
    prepare_user_info_for_return(user_info)
    return jsonify(**user_info)


def set_session(user_info):
    """
    Sets client session
    :param user_info: user info dict
    :return: None
    """
    session[T.F.id_user] = user_info[T.F.login]
    session[T.F.is_operator] = bool(user_info[T.F.is_operator])


@app.route('/connectfb', methods=['POST'])
@guard
def url_connect_fb():
    """
    Connects the logged in user account to a facebook account whose id is supplied in the json.
    """
    id_user = session.get(T.F.id_user, None)
    if id_user is None:
        return unauthorized()
    form = {T.F.fb_id : basestring}
    if not is_well_formed(request.json, form):
        return bad_request()
    hashed_id = hashlib.sha512(str(request.json[T.F.fb_id])).hexdigest()
    if not connect_fb(hashed_id, id_user):
        return conflict()
    return OK()


def connect_fb(hashed_fb_id, id_user):
    """
    Connects the user whose id is supplied with the facebook account whose hashed id is supplied
    :param basestring hashed_fb_id:
    :param basestring id_user:
    :return bool: whether the account was connected successfully
    """
    try:
        query.execute(g.db, Query.code.UPD_FB_USER, {T.F.fb_id: hashed_fb_id, T.F.id_user: id_user})
    except MySQLdb.IntegrityError:
        return False
    return True

    
@app.route('/user/edit', methods=['PUT'])
@guard
def url_user_edit():
    """
    Edits the logged in user with the supplied info in the json
    """
    id_user = session.get(T.F.id_user, None)
    if id_user is None:
        return unauthorized()
    if session.get(T.F.is_operator, False):
        if is_well_formed(request.json, OPERATOR_FORM):
            name = request.json[T.F.name_operator]
            pos_lat = request.json[T.F.position_latitude]
            pos_long = request.json[T.F.position_longitude]
            query.execute(g.db, Query.code.UPD_OPERATOR,
                          {T.F.name_operator: name, T.F.position_latitude: pos_lat, T.F.position_longitude: pos_long,
                           T.F.id_operator: id_user}, commit=False)
    if not edit_user_tags(id_user, request.json):
        return bad_request()
    g.db.commit()
    return OK()


@app.route('/logout')
def url_logout():
    """
    Logs out the requesting client
    """
    session.pop(T.F.id_user, None)
    session.pop(T.F.is_operator, None)
    return redirect(url_for('url_index'))

if __name__ == '__main__':
    #init_db()
    app.run()
