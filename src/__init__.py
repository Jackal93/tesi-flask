import os, sys
from flask import Flask

app = Flask(__name__)
app.secret_key = "development key"

# get in working directory
os.chdir('/home/web/daniele.cortesi2/html')
sys.stdout = open("log", "a")

import src.main
