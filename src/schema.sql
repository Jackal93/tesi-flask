create table User (
	login varchar(128) primary key,
    fb_id varchar(128) UNIQUE,
	psw varchar(128) NOT NULL,
	salt varchar(16) NOT NULL,
	is_operator tinyint(1) NOT NULL,
	name_operator varchar(50),
	position_latitude float,
	position_longitude float
);

create table Objective (
	id integer auto_increment primary key,
	name varchar(30) NOT NULL,
	description varchar(50),
	city varchar(60) NOT NULL,
	position_latitude float NOT NULL,
	position_longitude float NOT NULL,
	validation_method tinyint(1) NOT NULL,
	validation_code varchar(128) NOT NULL UNIQUE
);

create table Route (
	id integer primary key auto_increment,
	name varchar(40) NOT NULL,
	description varchar(50),
    id_operator varchar(128),
	validity_days integer,
	deleted tinyint(1) NOT NULL,
	foreign key (id_operator)
		references User(login),
);

create table obj_route (
	id_obj integer,
	id_route integer,
	primary key (id_obj, id_route),
	foreign key (id_obj)
		references Objective(id),
	foreign key (id_route)
		references Route(id)
);

create table Prize (
	id integer primary key auto_increment,
	id_operator varchar(128) NOT NULL,
	name varchar(30) NOT NULL,
	description varchar(50),
	visible tinyint(1) NOT NULL,
	repeat_days int NOT NULL,
	validation_method tinyint(1) NOT NULL,
	deleted tinyint(1) NOT NULL,
	foreign key (id_operator)
		references User(login)
);

create table Completion_info (
	id integer primary key auto_increment,
	probability integer(3)
);

create table Mandatory_obj (
	id_info integer,
	id_obj integer,
	primary key (id_info, id_obj),
	foreign key (id_info)
		references Completion_info(id),
	foreign key (id_obj)
		references Objective(id)
);

create table route_prize_info (
	id_route integer,
	id_prize integer,
	id_info integer NOT NULL,
	primary key (id_route, id_prize),

	foreign key (id_route)
		references Route(id),
	foreign key (id_prize)
		references Prize(id),
	foreign key (id_info)
		references Completion_info(id)
);

create table Completion_obj (
	id_obj integer,
	id_user varchar (128),
	date datetime,
	primary key (id_obj, id_user, date),
	foreign key (id_obj)
		references Objective(id),
	foreign key (id_user)
		references User(login)
);

create table Completion_prize (
	id_prize integer,
	id_user varchar (128),
	expiry_date datetime NOT NULL,
	validation_expiry_time datetime,
	validation_code varchar(128) UNIQUE,
	primary key (id_prize, id_user),
	foreign key (id_prize)
		references Prize(id),
	foreign key (id_user)
		references User(login)
);

create table Archive (
	id integer primary key auto_increment,
	id_prize integer NOT NULL,
	id_user varchar(128) NOT NULL,
	completion_date datetime NOT NULL,
    validation_code varchar(128) NOT NULL,
	foreign key (id_prize)
		references Prize(id),
	foreign key (id_user)
		references User(login)
);

create table Tag (
	id varchar(40) primary key
);

create table tag_user (
	id_tag varchar(40),
	id_user varchar(128),
	primary key (id_tag, id_user),
	foreign key (id_tag)
		references Tag(id),
	foreign key (id_user)
		references User(login)
);

create table tag_obj (
	id_tag varchar(40),
	id_obj integer,
	primary key (id_tag, id_obj),
	foreign key (id_tag)
		references Tag(id),
	foreign key (id_obj)
		references Objective(id)
);

create table tag_route (
	id_tag varchar(40),
	id_route integer,
	primary key (id_tag, id_route),
	foreign key (id_tag)
		references Tag(id),
	foreign key (id_route)
		references Route(id)
);

create view Route_available as
	select * from Route
	where deleted = 0;

create view Prize_available as
	select * from Prize
	where deleted = 0;

create view Prize_info as
	select id_route, id_prize, name, description, visible, repeat_days, validation_method, id_operator, name_operator,
		position_latitude, position_longitude, id_info, probability, id_obj
    from route_prize_info JOIN Completion_info I ON id_info = I.id NATURAL JOIN Mandatory_obj
    	JOIN Prize_available P ON id_prize = P.id JOIN User on id_operator = login;

show tables;
